import * as React from 'react';
import { View } from 'react-native';
import IQuoteScreenProps from '../interfaces/IQuoteScreenProps';
import hoistNonReactStatics from 'hoist-non-react-statics';

// State of the HOC you need to compute the InjectedProps
interface State {}

// Props you want the resulting component to take (besides the props of the wrapped component)
interface ExternalProps {}

// Props the HOC adds to the wrapped component
export interface InjectedProps {}

// Options for the HOC factory that are not dependent on props values
interface Options {
  key?: string;
}

export const lockable = ({ key = 'Default value' }: Options = {}) => <TOriginalProps extends IQuoteScreenProps>(
  Component: React.ComponentType<TOriginalProps & InjectedProps>
) => {
  // Do something with the options here or some side effects

  type ResultProps = TOriginalProps & ExternalProps;
  const result = class LockableComponent extends React.Component<ResultProps, State> {
    public static displayName = `Lockable(${Component.displayName || Component.name})`;

    constructor(props: ResultProps) {
      super(props);
      this.testGlobalTouch = this.testGlobalTouch.bind(this);
    }

    private testGlobalTouch() {
      console.log('global touch');
    }

    // Implement other methods here

    public render() {
      // Render all your added markup
      return (
        <View onTouchStart={this.testGlobalTouch} style={{ flex: 1 }}>
          <Component {...this.props} {...this.state} />
        </View>
      );
    }
  };

  hoistNonReactStatics(result, Component); // copy static methods
  return result;
};

// https://dev.to/danhomola/react-higher-order-components-in-typescript-made-simple
// https://reactjs.org/docs/composition-vs-inheritance.html
// https://reactjs.org/docs/higher-order-components.html
