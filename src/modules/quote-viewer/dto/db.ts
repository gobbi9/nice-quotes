import IUser from './interfaces/IUser';
import IQuote from './interfaces/IQuote';
import IComment from './interfaces/IComment';

export const userGobbi1: IUser = {
  id: 1,
  firstName: 'Guilherme',
  lastName: 'Gobbi',
  email: 'gobbi9@live.com',
  quotes: [],
  comments: []
};

export const quoteGobbi1: IQuote = {
  id: 1,
  text: 'Hello World',
  createdAt: new Date(2018, 9, 9, 4, 17, 0),
  user: userGobbi1,
  comments: []
};

export const quoteGobbi2: IQuote = {
  id: 2,
  text: 'Just wanna sleep today.',
  createdAt: new Date(2019, 1, 1, 2, 0, 0),
  user: userGobbi1,
  comments: []
};

export const quoteGobbi3: IQuote = {
  id: 3,
  text: 'Java is boring.',
  createdAt: new Date(2016, 11, 11, 12, 0, 0),
  user: userGobbi1,
  comments: []
};

export const commentGobbi1: IComment = {
  id: 1,
  content: ':)',
  createdAt: new Date(2018, 9, 19, 4, 17, 0),
  user: userGobbi1,
  quote: {} as IQuote
};

export const commentGobbi2: IComment = {
  id: 2,
  content: ':(',
  createdAt: new Date(2019, 12, 15, 14, 0, 0),
  user: userGobbi1,
  quote: {} as IQuote
};

export const commentGobbi3: IComment = {
  id: 3,
  content: 'Really?',
  createdAt: new Date(2019, 8, 12, 14, 0, 0),
  user: userGobbi1,
  quote: {} as IQuote
};

// -----------------------------------------------

export const userDoe2: IUser = {
  id: 2,
  firstName: 'John',
  lastName: 'Doe',
  email: 'john@doe.test',
  quotes: [],
  comments: []
};

export const quoteDoe4: IQuote = {
  id: 4,
  text: 'Generic quote...',
  createdAt: new Date(2017, 9, 19, 4, 17, 0),
  user: userDoe2,
  comments: []
};

export const quoteDoe5: IQuote = {
  id: 5,
  text: 'Another generic quote.',
  createdAt: new Date(2017, 1, 1, 2, 0, 0),
  user: userDoe2,
  comments: []
};

export const quoteDoe6: IQuote = {
  id: 6,
  text: 'Not really creative today.',
  createdAt: new Date(2016, 11, 11, 12, 0, 0),
  user: userDoe2,
  comments: []
};

export const commentDoe4: IComment = {
  id: 4,
  content: 'A test comment',
  createdAt: new Date(2019, 9, 19, 4, 17, 0),
  user: userDoe2,
  quote: {} as IQuote
};

export const commentDoe5: IComment = {
  id: 5,
  content: 'generic comment',
  createdAt: new Date(2019, 12, 15, 14, 0, 0),
  user: userDoe2,
  quote: {} as IQuote
};

export const commentDoe6: IComment = {
  id: 6,
  content: '???',
  createdAt: new Date(2019, 12, 30, 14, 0, 0),
  user: userDoe2,
  quote: {} as IQuote
};

// ------------

quoteGobbi1.comments.push(commentDoe4, commentDoe5);
commentDoe4.quote = quoteGobbi1;
commentDoe5.quote = quoteGobbi1;

quoteGobbi2.comments.push(commentDoe6);
commentDoe6.quote = quoteGobbi2;

quoteDoe4.comments.push(commentGobbi1, commentGobbi2);
commentGobbi1.quote = quoteDoe4;
commentGobbi2.quote = quoteDoe4;

quoteDoe5.comments.push(commentGobbi3);
commentGobbi3.quote = quoteDoe5;

userGobbi1.quotes.push(quoteGobbi1, quoteGobbi2, quoteGobbi3);
userGobbi1.comments.push(commentGobbi1, commentGobbi2, commentGobbi3);

userDoe2.quotes.push(quoteDoe4, quoteDoe5, quoteDoe6);
userDoe2.comments.push(commentDoe4, commentDoe5, commentDoe6);