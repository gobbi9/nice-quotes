import IUser from './IUser';
import IComment from './IComment';

export default interface IQuote {
  id: number;
  text: string;
  createdAt: Date;
  user: IUser;
  comments: Array<IComment>;
}