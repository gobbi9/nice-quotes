import IUser from './IUser';
import IQuote from './IQuote';

export default interface IComment {
  id: number;
  content: string;
  createdAt: Date;
  user: IUser;
  quote: IQuote;
}