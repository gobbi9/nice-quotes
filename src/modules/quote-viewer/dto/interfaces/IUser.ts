import IQuote from './IQuote';
import IComment from './IComment';

export default interface IUser {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  quotes: Array<IQuote>;
  comments: Array<IComment>;
}