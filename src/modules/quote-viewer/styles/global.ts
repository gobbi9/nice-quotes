import { StyleSheet } from 'react-native';
import IGlobalStyle from '../interfaces/IGlobalStyle';
import { NavigationStackScreenOptions } from 'react-navigation';
import { Theme } from '../enums/theme';

// https://github.com/vhpoet/react-native-styling-cheat-sheet
export const globalStyles = StyleSheet.create<IGlobalStyle>({
  body: {
    fontFamily: 'Roboto'
  }
});

// https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/react-navigation/v2/index.d.ts

export const globalNavigationProps = (theme: Theme = Theme.LIGHT): NavigationStackScreenOptions => {
  switch (theme) {
    case Theme.LIGHT:
      return globalNavigationPropsLight;
    case Theme.DARK:
      return globalNavigationPropsDark;
  }
};

const globalNavigationPropsLight: NavigationStackScreenOptions = {
  title: 'Good day',
  headerTintColor: '#444',
  headerStyle: {
    backgroundColor: 'azure',
    borderBottomColor: '#ffffff',
    borderBottomWidth: 3
  }
};

const globalNavigationPropsDark: NavigationStackScreenOptions = {
  ...globalNavigationPropsLight,
  title: 'Good evening',
  headerTintColor: '#eee',
  headerStyle: {
    borderBottomWidth: 3,
    backgroundColor: '#222',
    borderBottomColor: '#333'
  }
};
