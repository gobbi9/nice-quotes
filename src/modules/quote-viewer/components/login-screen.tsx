import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import ILoginScreenStyle from '../interfaces/ILoginScreenStyle';
import INavigationParams from '../interfaces/INavigationParams';
import { NavigationScreenProps } from 'react-navigation';
import { globalNavigationProps } from '../styles/global';
import IThemable from '../interfaces/IThemable';
import { connect } from 'react-redux';
import IQuoteViewerState from '../redux/model/IQuoteViewerState';
import { pickTheme } from '../redux/reducers/ui';

class LoginScreenRaw extends Component<NavigationScreenProps<INavigationParams> & IThemable> {
  public render() {
    return (
      <View
        style={styles.container}
        onTouchStart={() => this.props.navigation.navigate('Hello', globalNavigationProps(this.props.selectedTheme))}
      >
        <Text>Login</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create<ILoginScreenStyle>({
  container: {
    backgroundColor: 'azure',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
});

// retrieves properties FROM global state to the local component
const mapStateToProps = (state: IQuoteViewerState) => {
  return pickTheme(state);
};

// send properties TO the global state
const mapDispatchToProps = {
};

// connect to the redux store
const LoginScreen = connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginScreenRaw);

export default LoginScreen;
