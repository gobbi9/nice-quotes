import { StyleSheet, View } from 'react-native';
import React from 'react';
import Emoji from 'react-native-emoji';
import IHelloScreenStyle from '../interfaces/IHelloScreenStyle';
import IQuoteScreenProps from '../interfaces/IQuoteScreenProps';
import IQuoteScreenState from '../interfaces/IQuoteScreenState';
import { CustomText } from './custom-text';
import { Theme } from '../enums/theme';
import IQuoteViewerState from '../redux/model/IQuoteViewerState';
import { switchTheme } from '../redux/actions';
import { connect } from 'react-redux';
import { pickTheme } from '../redux/reducers/ui';
import NavigableComponent from './navigable-component';
import { globalNavigationProps } from '../styles/global';
import { NavigationScreenProps } from 'react-navigation';
import INavigationParams from '../interfaces/INavigationParams';
import { lockable } from '../hoc';

class HelloScreenRaw extends NavigableComponent<IQuoteScreenProps, IQuoteScreenState> {
  // override navigationOptions from parent class adding a new parameter
  public static navigationOptions = (navigationScreenProps: NavigationScreenProps<INavigationParams>) => {
    return {
      ...NavigableComponent.navigationOptions(navigationScreenProps),
      headerTitleStyle: navigationScreenProps.navigation.getParam('headerTitleStyle')
    };
  };

  private emojis = ['earth_africa', 'earth_americas', 'earth_asia'];

  constructor(props: IQuoteScreenProps) {
    super(props);

    // local state that is not passed to redux through props
    this.state = {
      emojiIndex: 0
    };

    this.switchEmote = this.switchEmote.bind(this);
  }

  private switchEmote() {
    this.setState({
      ...this.state,
      emojiIndex: (this.state.emojiIndex + 1) % this.emojis.length
    });
  }

  private styles = () => {
    switch (this.props.selectedTheme) {
      case Theme.LIGHT:
        return lightStyle;
      case Theme.DARK:
        return darkStyle;
    }
  };

  // comment out this whole method to see the difference
  protected overrideNavigationProps() {
    const globalNavProps = globalNavigationProps(this.props.selectedTheme);
    return {
      ...globalNavProps,
      title: `${globalNavProps.title} (${this.props.selectedTheme === Theme.DARK ? 'dark' : 'light'})`,
      // !!! this property must also be added to the navigationOptions
      headerTitleStyle: {
        fontFamily: 'monospace',
        fontSize: 16
      }
    };
  }

  // the parent class implements the following methods from React.Component to be able
  // to control the navigation properly, for this reason if you need to implement these methods
  // also on this class, the subclass, the method of the parent class must be called inside of it:

  // public componentDidMount() {
  //   super.componentDidMount();
  //   // your code here
  // }

  // public componentDidUpdate() {
  //   super.componentDidUpdate();
  //   // your code here
  // }

  // public shouldComponentUpdate(): boolean {
  //   return super.shouldComponentUpdate() && /*your condition here*/;
  // }

  // https://unicodey.com/emoji-data/table.htm
  public render() {
    return (
      <View style={this.styles().container} onTouchStart={this.switchTheme} onTouchEnd={this.switchEmote}>
        <CustomText style={this.styles().quote}>
          Hello World! <Emoji name={this.emojis[this.state.emojiIndex]} style={this.styles().emoji} />
        </CustomText>
        <CustomText style={this.styles().author}>- Guilherme Gobbi</CustomText>
      </View>
    );
  }
}

// https://medium.com/@zvona/react-native-and-typescript-meets-styles-b727ecf7e677

const lightStyle = StyleSheet.create<IHelloScreenStyle>({
  container: {
    backgroundColor: 'azure',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  quote: {
    fontSize: 24,
    fontStyle: 'italic',
    fontWeight: 'bold',
    color: '#444'
  },
  emoji: {
    fontStyle: 'normal',
    fontWeight: 'normal'
  },
  author: {
    fontSize: 20,
    fontStyle: 'italic',
    color: '#666'
  }
});

// darkStyle "extends" lightStyle
const darkStyle = StyleSheet.create<IHelloScreenStyle>({
  container: {
    ...lightStyle.container,
    backgroundColor: '#222'
  },
  quote: {
    ...lightStyle.quote,
    color: '#eee'
  },
  emoji: {
    ...lightStyle.emoji
  },
  author: {
    ...lightStyle.author,
    color: '#ccc'
  }
});

// redux boilerplate code

// retrieves properties FROM global state to the local component
const mapStateToProps = (state: IQuoteViewerState) => {
  return pickTheme(state);
};

// send properties TO the global state
const mapDispatchToProps = {
  switchTheme: switchTheme
};

// connect to the redux store
const HelloScreen = connect(
  mapStateToProps,
  mapDispatchToProps
)(lockable()(HelloScreenRaw));

export default HelloScreen;
