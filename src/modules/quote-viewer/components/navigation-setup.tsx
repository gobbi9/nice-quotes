import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation';
import HelloScreen from './hello-screen';
import LoginScreen from './login-screen';
import { Easing, Animated } from 'react-native';

// https://medium.com/@dooboolab/types-of-navigation-in-react-navigation-v2-bce6d24d94ec
// https://reactnavigation.org/docs/en/getting-started.html

const mainContainer = createStackNavigator(
  {
    Login: {
      screen: LoginScreen,
      path: '/',
      navigationOptions: (/*navigationOptions: NavigationScreenProps<INavigationParams>*/) => ({
        // tslint:disable-next-line:no-null-keyword
        header: null
      })
    },
    Hello: {
      screen: HelloScreen,
      path: '/hello'
      // , navigationOptions: () => globalNavigationProps(initialQuoteViewerState.ui.selectedTheme)
    }
  },
  {
    initialRouteName: 'Login',
    defaultNavigationOptions: { gesturesEnabled: false },
    transitionConfig: () => ({
      transitionSpec: {
        duration: 1000,
        easing: Easing.out(Easing.poly(4)),
        timing: Animated.timing
      },
      screenInterpolator: (sceneProps) => {
        const { layout, position, scene } = sceneProps;
        const { index } = scene;

        const height = layout.initHeight;
        const translateY = position.interpolate({
          inputRange: [index - 1, index, index + 1],
          outputRange: [height, 0, 0]
        });

        const opacity = position.interpolate({
          inputRange: [index - 1, index - 0.99, index],
          outputRange: [0, 1, 1]
        });

        return { opacity, transform: [{ translateY }] };
      }
    })
  }
);

const Navigator = createAppContainer(mainContainer);

export default Navigator;

// https://reactnavigation.org/docs/en/redux-integration.html

// https://expo.io/@react-navigation/NavigationPlayground
// https://github.com/react-navigation/react-navigation/tree/master/examples/NavigationPlayground
