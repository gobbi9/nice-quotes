import { Component } from 'react';
import INavigationParams from '../interfaces/INavigationParams';
import { NavigationScreenProps, NavigationStackScreenOptions } from 'react-navigation';
import { globalNavigationProps } from '../styles/global';
import IThemable from '../interfaces/IThemable';

// class that enables changing dynamically the navigation properties based on a theme
export default abstract class NavigableComponent<
  P extends IThemable & NavigationScreenProps<INavigationParams>,
  S,
  SS = {}
> extends Component<P, S, SS> {
  // props.navigation.setParams triggers componentDidUpdate causing an infinite loop,
  // thats why this flag is necessary
  private navigationTitleUpToDate = false;

  public static navigationOptions = (navigationScreenProps: NavigationScreenProps<INavigationParams>) => {
    return {
      // https://reactnativecode.com/dynamically-change-static-navigationoptions-value/
      // navigation bar properties that can be dynamically changed MUST be listed here
      title: navigationScreenProps.navigation.getParam('title'),
      headerTintColor: navigationScreenProps.navigation.getParam('headerTintColor'),
      headerStyle: navigationScreenProps.navigation.getParam('headerStyle')
    };
  };

  constructor(props: P) {
    super(props);
    this.switchTheme = this.switchTheme.bind(this);
  }

  // http://busypeoples.github.io/post/react-component-lifecycle/

  public componentDidMount() {
    this.navigationTitleUpToDate = false;
    // console.debug('did mount');
    this.updateNavigationTitle();
  }

  public componentDidUpdate() {
    // console.debug('did update');
    this.updateNavigationTitle();
  }

  // possible params: nextProps, prevState
  public shouldComponentUpdate(): boolean {
    const previousNavigationTitleUpToDateVariable = this.navigationTitleUpToDate;
    this.navigationTitleUpToDate = false;
    return !previousNavigationTitleUpToDateVariable;
  }

  public updateNavigationTitle() {
    this.navigationTitleUpToDate = true;
    this.props.navigation.setParams(this.overrideNavigationProps());
  }

  protected switchTheme(event: any) {
    this.navigationTitleUpToDate = false;
    this.props.switchTheme();
  }

  // override this method to personalise the navigation props
  protected overrideNavigationProps(): NavigationStackScreenOptions {
    return globalNavigationProps(this.props.selectedTheme);
  }
}
