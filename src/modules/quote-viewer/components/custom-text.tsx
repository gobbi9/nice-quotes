import { Text, TextProps } from 'react-native';
import React, { Component } from 'react';
import { globalStyles } from '../styles/global';

export class CustomText extends Component<TextProps> {
  constructor(props: TextProps) {
    super(props);
  }

  public render() {
    return <Text style={[globalStyles.body, this.props.style]}> {this.props.children} </Text>;
  }
}
