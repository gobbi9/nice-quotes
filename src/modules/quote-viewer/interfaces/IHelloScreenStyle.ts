import { TextStyle, ViewStyle } from 'react-native';
import IStyle from './IStyle';

export default interface IHelloScreenStyle extends IStyle {
  container: ViewStyle;
  quote: TextStyle;
  emoji: TextStyle;
  author: TextStyle;
  // property: ViewStyle | TextStyle | FlexStyle
  // invalidProperty: string // not valid because of the type enforcement of IStyle
}
