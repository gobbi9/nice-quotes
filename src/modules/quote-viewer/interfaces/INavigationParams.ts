import { NavigationStackScreenOptions } from 'react-navigation';

export default interface INavigationParams extends NavigationStackScreenOptions {}