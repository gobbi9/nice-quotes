import { ViewStyle, TextStyle, FlexStyle } from 'react-native';

// well it's complicated: this Interface enforces that all properties of the implemented interfaces/classes
// must have the types listed in here, see IQuoteScreenStyle.ts for an example
export default interface IStyle {
  [_: string]: ViewStyle | FlexStyle | TextStyle;
}
