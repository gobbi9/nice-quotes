import IThemable from './IThemable';
import INavigationParams from './INavigationParams';
import { NavigationScreenProps } from 'react-navigation';

export default interface IQuoteScreenProps extends IThemable, NavigationScreenProps<INavigationParams> {}
