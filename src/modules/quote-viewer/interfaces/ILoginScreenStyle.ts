import IStyle from './IStyle';
import { ViewStyle } from 'react-native';

export default interface ILoginScreenStyle extends IStyle {
  container: ViewStyle;
}