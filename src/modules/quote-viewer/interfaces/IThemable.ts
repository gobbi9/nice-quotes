import { Theme } from '../enums/theme';
import IAction from '../redux/actions/IAction';

export default interface IThemable {
  selectedTheme: Theme;
  switchTheme(): IAction;
}