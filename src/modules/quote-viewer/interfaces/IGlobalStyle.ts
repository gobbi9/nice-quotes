import { TextStyle } from 'react-native';

export default interface IGlobalStyle {
  body: TextStyle;
}
