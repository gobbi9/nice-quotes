import { QuoteAction } from '../enums/actions';

export default interface IAction {
  type: QuoteAction;
  payload?: any;
  response?: any;
}
