import IAction from './IAction';
import { QuoteAction } from '../enums/actions';

export function switchTheme(): IAction {
  return { type: QuoteAction.CHANGE_THEME };
}
