import { initialQuoteViewerState } from '../model/IQuoteViewerState';
import IAction from '../actions/IAction';
import IEntityTypes from '../model/IEntityTypes';

const initialEntitiesState = initialQuoteViewerState.entities;

export default function entitiesReducer(state: IEntityTypes = initialEntitiesState, action: IAction) {
  return state;
}
