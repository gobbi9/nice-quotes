import IUi from '../model/IUi';
import IQuoteViewerState, { initialQuoteViewerState } from '../model/IQuoteViewerState';
import IAction from '../actions/IAction';
import { QuoteAction } from '../enums/actions';
import produce from 'immer';
import { Theme } from '../../enums/theme';

const initialUiState = initialQuoteViewerState.ui;

export default function uiReducer(state: IUi = initialUiState, action: IAction) {
  if (action.type === QuoteAction.CHANGE_THEME) {
    return produce(state, (draft) => {
      draft.selectedTheme = switchTheme(state.selectedTheme);
    });
  }

  return state;
}

function switchTheme(theme?: Theme): Theme {
  const newTheme: Theme = theme || Theme.LIGHT;
  switch (theme) {
    case Theme.LIGHT:
      return Theme.DARK;
    case Theme.DARK:
      return Theme.LIGHT;
  }
  return newTheme;
}

export function pickTheme(globalState: IQuoteViewerState) {
  return { selectedTheme: globalState.ui.selectedTheme };
}
