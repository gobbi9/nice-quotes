import { combineReducers } from 'redux';
import IQuoteViewerState from '../model/IQuoteViewerState';
import uiReducer from './ui';
import entitiesReducer from './entities';

const rootReducer = combineReducers<IQuoteViewerState>({
  ui: uiReducer,
  entities: entitiesReducer
});

export default rootReducer;

export type AppState = ReturnType<typeof rootReducer>;
