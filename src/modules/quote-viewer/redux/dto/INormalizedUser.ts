export default interface INormalizedUser {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  quotes: Array<string>;
  comments: Array<string>;
}