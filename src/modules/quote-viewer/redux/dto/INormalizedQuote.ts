export default interface INormalizedQuote {
  id:string;
  text: string;
  createdAt: Date;
  user:string;
  comments: Array<string>;
}