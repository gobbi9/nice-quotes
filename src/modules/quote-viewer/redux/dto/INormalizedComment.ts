export default interface INormalizedComment {
  id: string;
  content: string;
  createdAt: Date;
  user: string;
  quote: string;
}