import INormalizedUser from './dto/INormalizedUser';
import INormalizedQuote from './dto/INormalizedQuote';
import INormalizedComment from './dto/INormalizedComment';

export const userGobbi1: INormalizedUser = {
  id: '1',
  firstName: 'Guilherme',
  lastName: 'Gobbi',
  email: 'gobbi9@live.com',
  quotes: ['1', '2', '3'],
  comments: ['1', '2', '3']
};

export const quoteGobbi1: INormalizedQuote = {
  id: '1',
  text: 'Hello World',
  createdAt: new Date(2018, 9, 9, 4, 17, 0),
  user: '1',
  comments: ['4', '5']
};

export const quoteGobbi2: INormalizedQuote = {
  id: '2',
  text: 'Just wanna sleep today.',
  createdAt: new Date(2019, 1, 1, 2, 0, 0),
  user: '1',
  comments: ['6']
};

export const quoteGobbi3: INormalizedQuote = {
  id: '3',
  text: 'Java is boring.',
  createdAt: new Date(2016, 11, 11, 12, 0, 0),
  user: '1',
  comments: []
};

export const commentGobbi1: INormalizedComment = {
  id: '1',
  content: ':)',
  createdAt: new Date(2018, 9, 19, 4, 17, 0),
  user: '1',
  quote: '4'
};

export const commentGobbi2: INormalizedComment = {
  id: '2',
  content: ':(',
  createdAt: new Date(2019, 12, 15, 14, 0, 0),
  user: '1',
  quote: '4'
};

export const commentGobbi3: INormalizedComment = {
  id: '3',
  content: 'Really?',
  createdAt: new Date(2019, 8, 12, 14, 0, 0),
  user: '1',
  quote: '5'
};

// -----------------------------------------------

export const userDoe2: INormalizedUser = {
  id: '2',
  firstName: 'John',
  lastName: 'Doe',
  email: 'john@doe.test',
  quotes: ['4', '5', '6'],
  comments: ['4', '5', '6']
};

export const quoteDoe4: INormalizedQuote = {
  id: '4',
  text: 'Generic quote...',
  createdAt: new Date(2017, 9, 19, 4, 17, 0),
  user: '2',
  comments: ['1', '2']
};

export const quoteDoe5: INormalizedQuote = {
  id: '5',
  text: 'Another generic quote.',
  createdAt: new Date(2017, 1, 1, 2, 0, 0),
  user: '2',
  comments: ['3']
};

export const quoteDoe6: INormalizedQuote = {
  id: '6',
  text: 'Not really creative today.',
  createdAt: new Date(2016, 11, 11, 12, 0, 0),
  user: '2',
  comments: []
};

export const commentDoe4: INormalizedComment = {
  id: '4',
  content: 'A test comment',
  createdAt: new Date(2019, 9, 19, 4, 17, 0),
  user: '2',
  quote: '1'
};

export const commentDoe5: INormalizedComment = {
  id: '5',
  content: 'generic comment',
  createdAt: new Date(2019, 12, 15, 14, 0, 0),
  user: '2',
  quote: '1'
};

export const commentDoe6: INormalizedComment = {
  id: '6',
  content: '???',
  createdAt: new Date(2019, 12, 30, 14, 0, 0),
  user: '2',
  quote: '2'
};

// ------------

export const allUsers = new Array(userGobbi1, userDoe2);
export const allComments = new Array(commentGobbi1, commentGobbi2, commentGobbi3, commentDoe4, commentDoe5, commentDoe6);
export const allQuotes = new Array(quoteGobbi1, quoteGobbi2, quoteGobbi3, quoteDoe4, quoteDoe5, quoteDoe6);

// quoteGobbi1.comments.push('4', '5');
// commentDoe4.quote = '1';
// commentDoe5.quote = '1';

// quoteGobbi2.comments.push('6');
// commentDoe6.quote = '2';

// quoteDoe4.comments.push('1', '2');
// commentGobbi1.quote = '4';
// commentGobbi2.quote = '4';

// quoteDoe5.comments.push('3');
// commentGobbi3.quote = '5';

// userGobbi1.quotes.push('1', '2', '3');
// userGobbi1.comments.push('1', '2', '3');

// userDoe2.quotes.push('4', '5', '6');
// userDoe2.comments.push('4', '5', '6');
