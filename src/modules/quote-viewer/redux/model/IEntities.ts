import INormalizedCollection from './INormalizedCollection';
import INormalizedUser from '../dto/INormalizedUser';
import INormalizedQuote from '../dto/INormalizedQuote';
import INormalizedComment from '../dto/INormalizedComment';

// T: entity type
export default interface IEntities {
  users?: INormalizedCollection<INormalizedUser>;
  quotes?: INormalizedCollection<INormalizedQuote>;
  comments?: INormalizedCollection<INormalizedComment>;
}