import { Theme } from '../../enums/theme';
import IAction from '../actions/IAction';

export default interface IUi {
  selectedTheme: Theme;
  selectedQuoteId: string;
  loggedUserId: string;
  switchTheme?(): IAction;
}