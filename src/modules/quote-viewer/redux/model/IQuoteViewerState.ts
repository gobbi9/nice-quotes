import INormalizedUser from '../dto/INormalizedUser';
import INormalizedCollection from './INormalizedCollection';
import INormalizedComment from '../dto/INormalizedComment';
import INormalizedQuote from '../dto/INormalizedQuote';
import { Theme } from '../../enums/theme';
import { allUsers, allQuotes, allComments } from '../db-norm';
import INormalizedEntity from './INormalizedEntity';
import IUi from './IUi';
import IEntityTypes from './IEntityTypes';

export default interface IQuoteViewerState {
  ui: IUi;
  entities: IEntityTypes;
}

// creates from 'elements' = [obj1, obj2] the object {"id1": obj1, "id2", obj2}
export function generateNormalizedObjectsIndexedById<T>(
  elements: Array<T & { id: string }>
): INormalizedEntity<T & { id: string }> {
  const response: any = {};
  elements.forEach((element: T & { id: string }) => {
    response[element.id] = element;
  });
  return response;
}

export function extractIds<T>(elements: Array<T & { id: string }>): Array<string> {
  return elements.map((e: T & { id: string }) => e.id);
}

export function normalizeCollection<T>(elements: Array<T & { id: string }>): INormalizedCollection<T> {
  return {
    byId: generateNormalizedObjectsIndexedById<T>(elements),
    allIds: extractIds<T>(elements)
  };
}

export const initialQuoteViewerState: IQuoteViewerState = {
  ui: { selectedTheme: Theme.LIGHT, selectedQuoteId: '1', loggedUserId: '1' },
  entities: {
    current: {
      users: normalizeCollection<INormalizedUser>(allUsers),
      quotes: normalizeCollection<INormalizedQuote>(allQuotes),
      comments: normalizeCollection<INormalizedComment>(allComments)
    },
    wip: {}
  }
};
