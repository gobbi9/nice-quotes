import INormalizedEntity from './INormalizedEntity';

// T: type of the entity
export default interface INormalizedCollection<T> {
  byId: INormalizedEntity<T>;
  allIds: Array<string>;
}