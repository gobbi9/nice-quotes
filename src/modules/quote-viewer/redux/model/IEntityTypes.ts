import IEntities from './IEntities';

export default interface IEntityTypes {
  current: IEntities;
  wip: IEntities; // when a entity is editing mode
}