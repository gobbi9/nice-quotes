// T: entity type

export default interface INormalizedEntity<T> {
  [_: string]: T;
}
