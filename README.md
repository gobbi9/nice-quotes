# Nice Quotes Project React Native App - Anweisungen zur Installation

## Voraussetzungen

- [node installieren](https://nodejs.org/en/download/)
- [yarn installieren](https://yarnpkg.com/lang/en/docs/install)
- expo-cli global installieren: `yarn global add expo-cli`

## Anwendung starten

1. Dependencies herunterladen lassen: `yarn install`
2. `yarn start` oder `yarn dev`

> Bitte nur yarn statt npm verwenden, um die Dependencies zu installieren

## Code Konventionen

- IDE Vorschlag: VsCode mit den Plugins *prettier*, *react native tools*, *Syntax Highlight for yarn.lock*
- Code automatisch mit prettier formattieren lassen: `Ctrl` + `Shift` + `P` > `Format Code`

## Besondere Anmerkungen

- Die Parameter bei einem Alias Befehl werden weitergegeben, d.h. um `yarn dev` bsp. auf IOS laufen zu lassen, muss man einfach `yarn dev --ios` eingeben
- **Nach Verändern/Hinzufügen eines Umgebungsparameters (API_URL, NODE_ENV) in package.json muss der Cache geleert werden**, das kann erreicht werden, indem man das Parameter `-c` eingibt, z.B. `yarn start -c`, `yarn local -c`, `yarn dev -c` oder `yarn start -c --ios`.

---

README vom Basisprojekt: <https://github.com/janaagaard75/expo-and-typescript>