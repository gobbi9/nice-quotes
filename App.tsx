import Navigator from './src/modules/quote-viewer/components/navigation-setup';
import { createStore } from 'redux';
import rootReducer from './src/modules/quote-viewer/redux/reducers';
import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { AppRegistry } from 'react-native';

const store = createStore(rootReducer);

export default class App extends Component {
  // constructor(props: any) {
  //   super(props);
  //   this.testGlobalTouch = this.testGlobalTouch.bind(this);
  // }

  // private testGlobalTouch() {
  //   console.log('global touch');
  // }

  public render() {
    return (
      <Provider store={store}>
        {/* <View onTouchStart={this.testGlobalTouch} style={{ flex: 1 }}> */}
          <Navigator />
        {/* </View> */}
      </Provider>
    );
  }
}

AppRegistry.registerComponent('App', () => App);

if (!__DEV__) {
  // hide all logging messages in a productive environment
  // because they are slow and can be reversed engineered
  // and read by the user
  console.log = () => {};
  console.debug = () => {};
  console.error = () => {};
  console.info = () => {};
  console.trace = () => {};
  console.warn = () => {};
  const msg = 'should not be displayed';
  console.log(msg);
  console.debug(msg);
  console.error(msg);
  console.info(msg);
  console.trace(msg);
  console.warn(msg);
}

console.log(`API_URL=${process.env.API_URL}, NODE_ENV=${process.env.NODE_ENV}`);
